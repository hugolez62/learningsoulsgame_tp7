package lsg.armor;

public class DragonSlayerLeggings extends ArmorItem {

    private static int WEIGHT = 3;


    public DragonSlayerLeggings() {
        super("Dragon Slayer Leggings", 10.2f);
    }

    @Override
    public int getWeight() {
        return WEIGHT;
    }
}
