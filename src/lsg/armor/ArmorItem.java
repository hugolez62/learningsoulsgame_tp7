package lsg.armor;

import lsg.bags.Collectible;

public class ArmorItem implements Collectible {

    private String name;
    private float armorValue;

    private static String PROTECT_STAT_STRING= "PROTECTION:";
    private static int WEIGHT = 4;


    public ArmorItem(String name, float armorValue) {
        this.name = name;
        this.armorValue = armorValue;
    }

    public String getName() {
        return name;
    }

    public float getArmorValue() {
        return armorValue;
    }

    public String toString() {
        return name + "(" + armorValue + ")";
    }

    public int getWeight() {
        return WEIGHT;
    }
}
