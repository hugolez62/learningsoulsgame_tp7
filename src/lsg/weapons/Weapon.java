package lsg.weapons;

import lsg.bags.Collectible;
import lsg.consumables.repair.RepairKit;

public class Weapon implements Collectible {

    private String name;
    private int minDamage;
    private int maxDamage;
    private int stamCost;
    private int durability;

    public static String DURABILITY_STAT_STRING = "durability";
    private static int WEIGHT = 2;

    public Weapon(String name, int minDamage, int maxDamage, int stamCost, int durability) {

        this.name = name;
        this.minDamage = minDamage;
        this.maxDamage = maxDamage;
        this.stamCost = stamCost;
        this.durability = durability;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMinDamage() {
        return minDamage;
    }

    public void setMinDamage(int minDamage) {
        this.minDamage = minDamage;
    }

    public int getMaxDamage() {
        return maxDamage;
    }

    public void setMaxDamage(int maxDamage) {
        this.maxDamage = maxDamage;
    }

    public int getStamCost() {
        return stamCost;
    }

    public void setStamCost(int stamCost) {
        this.stamCost = stamCost;
    }

    public int getDurability() {
        return durability;
    }

    private void setDurability(int durability) {
        this.durability = durability;
    }

    public void use() {
        this.durability--;
    }

    public boolean isBroken() {
        return durability <= 0;
    }

    public void repairWith(RepairKit kit) {
        int repairValue = kit.use();
        this.durability = this.durability + repairValue;
    }

    public String toString() {

        String value = this.name+" ";

        value += "(min:" + this.minDamage;
        value += " max:" + this.maxDamage;
        value += " stam:" + this.stamCost;
        value += " dur:" + this.durability;
        value += ")";

        return value;
    }

    public void printStats() {
        System.out.println(this.toString());
    }

    public int getWeight() {
        return WEIGHT;
    }
}
