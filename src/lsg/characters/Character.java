package lsg.characters;

import lsg.armor.ArmorItem;
import lsg.armor.DragonSlayerLeggings;
import lsg.armor.RingedKnightArmor;
import lsg.bags.Bag;
import lsg.bags.Collectible;
import lsg.bags.SmallBag;
import lsg.consumables.Consumable;
import lsg.consumables.drinks.Drink;
import lsg.consumables.food.Food;
import lsg.consumables.repair.RepairKit;
import lsg.exceptions.*;
import lsg.helpers.Dice;
import lsg.weapons.ShotGun;
import lsg.weapons.Weapon;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public abstract class Character {

    private static final String LIFE_STAT_STRING = " LIFE:";
    private static final String STAM_STAT_STRING = " STAMINA:";
    public static final String BUFF_STAT_STRING = " BUFF:";
    public static final String PROTECTION_STAT_STRING = "PROTECTION:";
    private String name;
    private int life;
    private int maxLife;
    private int stamina;
    private int maxStamina;
    private Dice dice;
    private Weapon weapon;
    private Consumable consumable;
    private Bag bag;

    public Character() {
        this.bag = new SmallBag();
    }

    public static void main(String[] args) {

        Hero hero = new Hero();

        ArmorItem item1 = new RingedKnightArmor();
        ArmorItem item2 = new DragonSlayerLeggings();
        Weapon arme1 = new ShotGun();

        hero.printBag();


    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public int getMaxLife() {
        return maxLife;
    }

    public void setMaxLife(int maxLife) {
        this.maxLife = maxLife;
    }

    public int getStamina() {
        return stamina;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    public int getMaxStamina() {
        return maxStamina;
    }

    public void setMaxStamina(int maxStamina) {
        this.maxStamina = maxStamina;
    }

    public Dice getDice() {
        return dice;
    }

    public void setDice(Dice dice) {
        this.dice = dice;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public Consumable getConsumable() {
        return consumable;
    }

    public void setConsumable(Consumable consumable) {
        this.consumable = consumable;
    }

    public String toString() {

        NumberFormat formatter = NumberFormat.getInstance(Locale.US);
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);
        formatter.setRoundingMode(RoundingMode.HALF_UP);

        DecimalFormat decimalFormat = (DecimalFormat) formatter;
        decimalFormat.setMinimumFractionDigits(2);
        decimalFormat.setMaximumFractionDigits(2);
        decimalFormat.setRoundingMode(RoundingMode.HALF_UP);
        String formatedProtection = decimalFormat.format(computeProtection());
        String formatedBuff = decimalFormat.format(computeBuff());

        String value = String.format("%-20s ", "[ " + this.getClass().getSimpleName() + " ]");

        value += String.format("%-20s", this.name);
        value += String.format("%-6s", LIFE_STAT_STRING);
        value += String.format("%-10s", String.format("%5d", this.life));
        value += String.format("%-9s", STAM_STAT_STRING);
        value += String.format("%-11s", String.format("%5d", this.stamina));
        value += String.format("%-12s", "PROTECTION: ");
        value += String.format(Locale.US, "%-10s", String.format("%5s", formatedProtection));
        value += String.format("%-6s", "BUFF: ");
        value += String.format(Locale.US, "%-9s", String.format("%5s", formatedBuff));


        value += isAlive() ? "(ALIVE)" : "(DEAD)";

        return value;
    }

    public void printStats() {
        System.out.println(this.toString());
    }

    public boolean isAlive() {
        return this.life > 0;
    }

    public int attack() throws WeaponNullException, WeaponBrokenException, StaminaEmptyException {
        return attackWith(this.weapon);
    }

    private int attackWith(Weapon weapon) throws WeaponNullException, WeaponBrokenException, StaminaEmptyException {

        if (weapon == null) {
            throw new WeaponNullException();
        }

        if (weapon.isBroken()) {
            throw new WeaponBrokenException(weapon);
        }

        if (this.stamina == 0) {
            throw new StaminaEmptyException();
        }

        int dmgInfliges;

        if (this.stamina == 0) return 0;

        dmgInfliges = (dice.roll() * (weapon.getMaxDamage() - weapon.getMinDamage()) / 100) + weapon.getMinDamage();

        if (this.stamina < weapon.getStamCost()) {
            this.stamina = 0;
            dmgInfliges = dmgInfliges * ((100 * this.stamina / this.weapon.getStamCost()) / 100);
        } else {
            this.stamina = this.stamina - this.weapon.getStamCost();
        }

        dmgInfliges = (dmgInfliges * (((int) this.computeBuff() / 100) + 1));

        this.weapon.use();

        return Math.round(dmgInfliges);
    }

    public int getHitWith(int dmg) {

        int dmgCalcul;
        int dmgValue;

        float protectValue = this.computeProtection();

        dmgCalcul = (protectValue > 100) ? 0 : Math.round(dmg - (dmg * protectValue / 100));
        dmgValue = (this.life > dmgCalcul) ? dmgCalcul : this.life;
        this.life = this.life - dmgValue;

        return dmgValue;

    }

    private void drink(Drink drink) throws ConsumeNullException, ConsumeEmptyException {

        if (drink == null) {
            throw new ConsumeNullException();
        }

        System.out.println(this.name + " drinks " + drink.toString());
        int valueStamina = drink.use();
        this.stamina = (this.stamina + valueStamina > this.maxStamina) ? this.maxStamina : this.stamina + valueStamina;
    }

    private void eat(Food food) throws ConsumeNullException, ConsumeEmptyException {

        if (food == null) {
            throw new ConsumeNullException();
        }

        System.out.println(this.name + " eats " + food.toString());
        int valueLife = food.use();
        this.life = (this.life + valueLife > this.maxLife) ? this.maxLife : this.life + valueLife;
    }

    private void use(Consumable consumable) throws ConsumeNullException, ConsumeEmptyException, ConsumeRepairNullWeaponException {

        if (consumable == null) {
            throw new ConsumeNullException();
        }

        if (consumable instanceof Drink) {
            drink((Drink) consumable);
        } else if (consumable instanceof Food) {
            eat((Food) consumable);
        } else if (consumable instanceof RepairKit) {
            try {
                repairWeaponWith((RepairKit) consumable);
            } catch (WeaponNullException e) {
                throw new ConsumeRepairNullWeaponException();
            }
        }
    }

    private void repairWeaponWith(RepairKit kit) throws WeaponNullException {
        if (this.getWeapon() == null) {
            throw new WeaponNullException();
        }
        System.out.println(this.name + " repairs " + this.getWeapon().toString() + " with " + kit.toString());
        this.getWeapon().repairWith(kit);
    }

    public void consume() throws ConsumeNullException, ConsumeEmptyException, ConsumeRepairNullWeaponException {
        use(this.consumable);
    }

    public void pickUp(Collectible item) throws NoBagException {

        if(bag == null) {
            throw new NoBagException();
        }

        if (this.bag.getWeight() + item.getWeight() <= this.bag.getCapacity()) {
            try {
                bag.push(item);
            } catch (BagFullException e) {
                e.printStackTrace();
            }
            System.out.print(this.name + " picks up " + item.toString());
        }
    }

    public Collectible pullOut(Collectible item) throws NoBagException {

        if(bag == null) {
            throw new NoBagException();
        }

        if (this.bag.contains(item)) {
            bag.pop(item);
            System.out.print(this.name + " pulls out " + item.toString());
            return item;
        } else {
            return null;
        }
    }

    public void printBag() {
        System.out.print("BAG : ");
        System.out.println(bag);
    }

    public void printWeapon() {
        System.out.print("WEAPON : ");
        try {
            this.getWeapon().printStats();
        } catch (Exception e) {
            System.out.println("null");
        }
    }

    public void printConsumable() {
        System.out.print("CONSUMABLE : ");
        try {
            this.getConsumable().printStats();
        } catch (Exception e) {
            System.out.println("null");
        }

    }

    public int getBagCapacity() throws NoBagException {
        if(bag == null) {
            throw new NoBagException();
        }

        return this.bag.getCapacity();
    }

    public int getBagWeight() throws NoBagException {
        if(bag == null) {
            throw new NoBagException();
        }
        return this.bag.getWeight();
    }

    public Collectible[] getBagItems() throws NoBagException {
        return this.bag.getItems();
    }

    public Bag getBag() {
        return bag;
    }

    public Bag setBag(Bag bag) {

        String bagName;
        Collectible[] bagItems = new Collectible[0];

        Bag oldBag = this.bag;

        if(bag == null) {
            System.out.println(this.name + " changes " + oldBag.getClass().getSimpleName() + " for null");
            this.bag = null;
        } else {
            try {
                bagItems = getBagItems();
            } catch (NoBagException e) {
                e.printStackTrace();
            }
            for (Collectible bagItem : bagItems) {
                if (bag.getWeight() + bagItem.getWeight() <= bag.getCapacity()) {
                    try {
                        bag.push(bagItem);
                    } catch (BagFullException e) {
                        e.printStackTrace();
                    }
                    oldBag.pop(bagItem);
                }
            }
            this.bag = bag;
            System.out.println(this.name + " changes " + oldBag.getClass().getSimpleName() + " for " + this.bag.getClass().getSimpleName());
        }

        return oldBag;
    }

    public void equip(Weapon weapon) throws NoBagException {
        if(bag == null) {
            throw new NoBagException();
        }
        if (this.bag.contains(weapon)) {
            this.setWeapon(weapon);
            pullOut(weapon);
        }
        System.out.println(" and equips it !");
    }

    public void equip(Consumable consumable) throws ConsumeNullException, ConsumeEmptyException, ConsumeRepairNullWeaponException, NoBagException {
        if(bag == null) {
            throw new NoBagException();
        }
        if (this.bag.contains(consumable)) {
            this.use(consumable);
            pullOut(consumable);
        }
        System.out.println(" and use it !");
    }

    private Consumable fastUseFirst(Class<? extends Consumable> type) throws ConsumeNullException, ConsumeEmptyException, ConsumeRepairNullWeaponException, NoBagException {

        Collectible[] bagItems = getBagItems();

        for (Collectible bagItem : bagItems) {
            if (type.isInstance(bagItem)) {
                use((Consumable) bagItem);
                int life = this.getLife();
                int maxLife = this.maxLife;
                int capacity = (((Consumable) bagItem).getCapacity());

                if (capacity <= 0) {
                    pullOut(bagItem);
                }
                System.out.println();
                return ((Consumable) bagItem);
            }
        }
        return null;
    }

    public Drink fastDrink() throws ConsumeNullException, ConsumeEmptyException, NoBagException {
        System.out.println(this.name + " drinks FAST :");
        try {
            return (Drink) fastUseFirst(Drink.class);
        } catch (ConsumeRepairNullWeaponException e) {
            return null;
        }
    }

    public Food fastEat() throws ConsumeNullException, ConsumeEmptyException, NoBagException {
        System.out.println(this.name + " eats FAST :");
        try {
            return (Food) fastUseFirst(Food.class);
        } catch (ConsumeRepairNullWeaponException e) {
            return null;
        }
    }

    public RepairKit fastRepair() throws ConsumeNullException, ConsumeEmptyException, ConsumeRepairNullWeaponException, NoBagException {
        System.out.println(this.name + " repairs FAST :");
        return (RepairKit) fastUseFirst(RepairKit.class);
    }

    protected abstract float computeProtection();

    protected abstract float computeBuff();
}
