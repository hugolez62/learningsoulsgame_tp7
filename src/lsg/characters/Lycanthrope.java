package lsg.characters;

import lsg.weapons.Claw;
import lsg.weapons.Sword;
import lsg.weapons.Weapon;

public class Lycanthrope extends Monster {

    private static final String DEFAULTNAME = "Lycanthrope";

    public Lycanthrope() {
        this.setName(DEFAULTNAME);
        this.setWeapon(new Claw());
        this.setSkinThickness(30);
    }
}
