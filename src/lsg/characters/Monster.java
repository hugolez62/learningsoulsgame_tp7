package lsg.characters;

import lsg.buffs.talismans.Talisman;
import lsg.helpers.Dice;

public class Monster extends Character {

    private static int DEFAULT_LIFE = 10;
    private static int DEFAULT_STAMINA = 10;
    private float skinThickness = 20;
    private static int INSTANCES_COUNT = 0;
    private static final String DEFAULTNAME = "Monster";
    private Talisman talisman;

    public Monster() {
        INSTANCES_COUNT++;
        this.setName(DEFAULTNAME + "_" + INSTANCES_COUNT);
        this.setLife(DEFAULT_LIFE);
        this.setMaxLife(DEFAULT_LIFE);
        this.setStamina(DEFAULT_STAMINA);
        this.setMaxStamina(DEFAULT_STAMINA);
        this.setDice(new Dice(101));
    }

    public Monster(String name) {
        INSTANCES_COUNT++;
        this.setName(name + "_" + INSTANCES_COUNT);
        this.setLife(DEFAULT_LIFE);
        this.setMaxLife(DEFAULT_LIFE);
        this.setStamina(DEFAULT_STAMINA);
        this.setMaxStamina(DEFAULT_STAMINA);
    }

    public float getSkinThickness() {
        return skinThickness;
    }

    protected void setSkinThickness(float SkinThickness) {
        skinThickness = SkinThickness;
    }

    public Talisman getTalisman() { return talisman; }

    public void setTalisman(Talisman Talisman) { talisman = Talisman; }

    private float getTotalBuff() {
        if(talisman == null) {
            return 0;
        }
        return talisman.computeBuffValue();
    }

    protected float computeProtection() {
        return getSkinThickness();
    }
    protected float computeBuff() {
        return getTotalBuff();
    }
}
