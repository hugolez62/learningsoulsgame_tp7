package lsg.buffs.rings;

import lsg.bags.Collectible;
import lsg.buffs.BuffItem;
import lsg.characters.Hero;

public abstract class Ring extends BuffItem implements Collectible {
	
	public int power ;
	public Hero hero ;
	
	public Ring(String name, int power) {
		super(name) ;
		this.power = power ;
	}
	
	public void setHero(Hero hero) {
		this.hero = hero;
	}
	
	public Hero getHero() {
		return hero;
	}

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public int getWeight() {
        return 1;
    }
}
