package lsg.exceptions;

import lsg.consumables.Consumable;

public abstract class ConsumeException extends Exception {

    String message;
    Consumable consumable;


    public ConsumeException(String message, Consumable consumable) {
        this.message = message;
        this.consumable = consumable;
    }

    public Consumable getConsumable() {
        return consumable;
    }

}
