package lsg.exceptions;

import lsg.weapons.Weapon;

public class WeaponBrokenException extends Exception {

    public WeaponBrokenException(Weapon weapon) {

        super(weapon.getName() + " is broken");
    }
}
