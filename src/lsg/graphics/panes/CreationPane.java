package lsg.graphics.panes;

import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import lsg.graphics.widgets.texts.GameLabel;

public class CreationPane extends VBox {

    private Scene scene;
    private TextField nameField;
    private static final Duration ANIMATION_DURATION = Duration.millis(1500);
    private static final double ZOOM_SCALE = 1.5;
    private static final double ZOOM_Y = 0.25;

    public CreationPane() {
        GameLabel titleTextField = new GameLabel("Player Name");
        this.getChildren().add(titleTextField);
        this.nameField = new TextField();
        this.getChildren().add(nameField);
        nameField.setAlignment(Pos.CENTER);
        titleTextField.setStyle("-fx-padding: 0 0 20px 0");
        nameField.setMaxWidth(200);
    }

    public TextField getNameField() {
        return nameField;
    }

    public void fadeIn(EventHandler<ActionEvent> finishedHandler, CreationPane pane) {
        FadeTransition st = new FadeTransition();
        st.setNode(pane);
        st.setDuration(Duration.millis(1000));
        st.setFromValue(0);
        st.setToValue(1);
        st.setOnFinished(finishedHandler);
        st.setAutoReverse(true);

        st.play();
    }
}
