package lsg.graphics.widgets.texts;

import javafx.scene.Node;
import javafx.scene.control.Label;
import lsg.graphics.CSSFactory;

public class GameLabel extends Label {
    public GameLabel() {
        super();
        initCSS();
    }

    public GameLabel(String text) {
        super(text);
        initCSS();
    }

    public GameLabel(String text, Node graphic) {
        super(text, graphic);
        initCSS();
    }

    private void initCSS() {
        this.getStylesheets().add(CSSFactory.getStyleSheet("LSGFont.css"));
        this.getStyleClass().addAll("game-font", "game-font-fx");
    }
}
