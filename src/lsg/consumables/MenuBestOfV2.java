package lsg.consumables;

import lsg.consumables.drinks.Coffee;
import lsg.consumables.drinks.Whisky;
import lsg.consumables.drinks.Wine;
import lsg.consumables.food.Americain;
import lsg.consumables.food.Hamburger;

public class MenuBestOfV2 {

    private java.util.HashSet<Consumable> menu = new java.util.HashSet<Consumable> ();


    public MenuBestOfV2() {
        menu.add(new Hamburger());
        menu.add(new Wine());
        menu.add(new Americain());
        menu.add(new Coffee());
        menu.add(new Whisky());
    }

    public String toString() {
        StringBuilder value = new StringBuilder(this.getClass().getSimpleName() + " :\n");
        int i = 1;

        for (Consumable aMenu : menu) {
            value.append(i).append(" : ").append(aMenu).append("\n");
            i++;
        }

        return value.toString();
    }

    public static void main(String[] args) {

        MenuBestOfV2 menu = new MenuBestOfV2();
        System.out.println(menu.toString());
    }
}
