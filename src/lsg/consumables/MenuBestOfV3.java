package lsg.consumables;

import lsg.consumables.drinks.Coffee;
import lsg.consumables.drinks.Whisky;
import lsg.consumables.drinks.Wine;
import lsg.consumables.food.Americain;
import lsg.consumables.food.Hamburger;

import java.util.HashSet;
import java.util.Iterator;

public class MenuBestOfV3 extends HashSet<Consumable> {

    public MenuBestOfV3() {
        super.add(new Hamburger());
        super.add(new Wine());
        super.add(new Americain());
        super.add(new Coffee());
        super.add(new Whisky());
    }

    public String toString() {

        int i = 1;
        Iterator it = super.iterator();

        StringBuilder value = new StringBuilder(this.getClass().getSimpleName() + " :\n");

        while(it.hasNext()) {
            value.append(i).append(" : ").append(it.next()).append("\n");
            i++;
        }

        return value.toString();
    }

    public static void main(String[] args) {

        MenuBestOfV3 menu = new MenuBestOfV3();
        System.out.println(menu.toString());
    }
}