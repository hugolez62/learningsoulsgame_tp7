package lsg.consumables.repair;

import lsg.consumables.Consumable;
import lsg.weapons.Weapon;

public class RepairKit extends Consumable {

    public RepairKit() {
        super("Repair Kit", 10, Weapon.DURABILITY_STAT_STRING);
    }

    public int use() {
        int valeurInitiale = this.getCapacity();
        if(valeurInitiale > 0) {
            this.setCapacity(valeurInitiale - 1);
            return 1;
        } else {
            return 0;
        }
    }
}
