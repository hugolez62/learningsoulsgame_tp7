package lsg.bags;

import lsg.armor.ArmorItem;
import lsg.consumables.Consumable;
import lsg.weapons.Weapon;

public interface Collectible {

    public int getWeight();

    public String toString();
}
