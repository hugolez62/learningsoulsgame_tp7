package lsg.bags;

import lsg.armor.BlackWitchVeil;
import lsg.armor.DragonSlayerLeggings;
import lsg.armor.RingedKnightArmor;
import lsg.consumables.food.Hamburger;
import lsg.exceptions.BagFullException;
import lsg.weapons.ShotGun;
import lsg.weapons.Sword;

import java.util.HashSet;

public class Bag {

    private int capacity;
    private int weight;
    private HashSet<Collectible> items;
    public static final String BULLET_POINT = "\u2219";


    public Bag(int capacity) {
        this.capacity = capacity;
        this.weight = 0;
        this.items = new HashSet<Collectible>();
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void push(Collectible item) throws BagFullException {

        if(this.weight == this.capacity) {
            throw new BagFullException(this);
        }

        int weightItem = item.getWeight();
        if(this.weight + weightItem <= this.capacity) {
            items.add(item);
            this.weight = this.weight + weightItem;
        }
    }

    public Collectible pop(Collectible item) {
        int weightItem = item.getWeight();

        if(contains(item)) {
            items.remove(item);
            this.weight = this.weight - weightItem;
        }

        return item;
    }

    public boolean contains(Collectible item) {
        return items.contains(item);
    }

    public Collectible[] getItems() {

        Collectible[] tabItem;
        tabItem = new Collectible[items.size()];
        int i = 0;

        for (Collectible item: items) {
            tabItem[i] = item;
            i++;
        }

        return tabItem;
    }

    public static void transfer(Bag from, Bag into) throws BagFullException {

        if (from == null || into == null) {
            return;
        }

        Collectible[] itemsFrom = from.getItems();

        for (Collectible anItemsFrom : itemsFrom) {
            if(into.weight + anItemsFrom.getWeight() <= into.capacity) {
                into.push(anItemsFrom);
                from.pop(anItemsFrom);
            }
        }
    }

    @Override
    public String toString() {

        Collectible[] tabItem;
        tabItem = getItems();

        if(getItems().length == 0) {

            String value = this.getClass().getSimpleName() + " [ " + getItems().length + " items | " + this.weight + "/" + this.capacity + " kg ]\n";
            value += BULLET_POINT + " (empty)";

            return value;

        } else {

            String value = this.getClass().getSimpleName() + " [ " + getItems().length + " items | " + this.weight + "/" + this.capacity + " kg ]\n";
            for (int i = 0; i < getItems().length; i++) {
                value += BULLET_POINT + " " + tabItem[i].toString() + "[" + tabItem[i].getWeight() + " kg]\n";
            }

            return value;
        }
    }

    public static void main (String[] args) {
        Collectible collectible1 = new ShotGun();
        Collectible collectible2 = new DragonSlayerLeggings();
        Collectible collectible3 = new RingedKnightArmor();

        MediumBag bag= new MediumBag();


        System.out.println("Sac 1 :");
        System.out.println(bag.toString());

        SmallBag bag2 = new SmallBag();
        System.out.println("Sac 2 :");
        System.out.println(bag2.toString());

        System.out.println();

        System.out.println("Sac 2 après transfert :");
        System.out.println(bag2.toString());

        System.out.println("Sac 1 après transfert :");
        System.out.println(bag.toString());

    }


}
