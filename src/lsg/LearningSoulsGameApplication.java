package lsg;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import lsg.graphics.CSSFactory;
import lsg.graphics.ImageFactory;
import lsg.graphics.panes.AnimationPane;
import lsg.graphics.panes.CreationPane;
import lsg.graphics.panes.TitlePane;
import lsg.graphics.widgets.texts.GameLabel;

public class LearningSoulsGameApplication extends Application {

    private Scene scene;
    private AnchorPane root;
    private TitlePane gameTitle;
    private CreationPane creationPane;
    private String heroName;
    private AnimationPane animationPane;

    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Learning Souls Game");
        root = new AnchorPane();
        scene = new Scene(root, 1200, 800);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        buildUI();
        addListeners();
        primaryStage.show();
        startGame();
    }

    private void buildUI() {
        //this.scene.setUserAgentStylesheet(CSSFactory.getStyleSheet("LSG.css"));

        BackgroundImage myBI= new BackgroundImage(
                new Image("lsg/graphics/images/BACKGROUND.png",1200,800,false,true),
                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);
        this.root.setBackground(new Background(myBI));

        gameTitle = new TitlePane(scene, "Learning Souls Game");
        root.getChildren().add(gameTitle);
        AnchorPane.setLeftAnchor(gameTitle, 0.0);
        AnchorPane.setRightAnchor(gameTitle, 0.0);
        gameTitle.setAlignment(Pos.CENTER);
        creationPane = new CreationPane();
        root.getChildren().add(creationPane);
        AnchorPane.setLeftAnchor(creationPane, 0.0);
        AnchorPane.setRightAnchor(creationPane, 0.0);
        AnchorPane.setTopAnchor(creationPane, 0.0);
        AnchorPane.setBottomAnchor(creationPane, 0.0);
        creationPane.setAlignment(Pos.CENTER);
        this.creationPane.setStyle("-fx-opacity: 0;");
        this.animationPane = new AnimationPane(root);
    }

    private void play() {
        root.getChildren().add(this.animationPane);
        animationPane.startDemo();
    }

    private void addListeners() {
        creationPane.getNameField().setOnAction((event -> {
            heroName = creationPane.getNameField().getText();
            System.out.println("Nom du héro : " + heroName);
            if(heroName != null) {
                root.getChildren().remove(creationPane);
            }
            this.gameTitle.zoomOut(event2 -> {
                System.out.println("ZOOM initial !");
                play();
            });
        }));
    }

    private void startGame() {
        this.gameTitle.zoomIn(event -> {
            this.creationPane.fadeIn(event1 -> {
                ImageFactory.preloadAll((() -> {
                    System.out.println("Pré-chargement des images terminé");
                }));
            }, creationPane);
        });

    }
}
